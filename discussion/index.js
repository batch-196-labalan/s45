console.log("hi");

console.log(document);
//result: document HTML code

const txtFirstName = document.querySelector("#txt-first-name");

txtFirstName.addEventListener('keyup', txtFirstName);

// console.log(txtFirstName);
//result: input field/tag
//. >> targetting class

/*
	alternative ways:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1");

*/


// //target the full name
let spanFullName = document.querySelector("#span-full-name");


// //Event Listeners

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});


// // //target last name
const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value;
});



// // //breakdown meaning
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});




// //STRETCH

const keyCodeEvent = (e) => {
	let kc = e.keyCode;
	if(kc === 65){
		e.target.value = null;
		alert('Someone clicked a');
	}
}

// txtFirstName.addEventListener('keyup', keyCodeEvent);




//======ACTIVITY=========


const firstNameEvent = (e) => {
	spanFullName.innerHTML = txtFirstName.value + txtLastName.value;
}


const lastNameEvent = (e) => {
	spanFullName.innerHTML = txtFirstName.value + " "+ txtLastName.value;
}

txtFirstName.addEventListener('keyup', firstNameEvent);


txtLastName.addEventListener('keyup', lastNameEvent);










